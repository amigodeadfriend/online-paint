package test;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import page.OnlinePainterPage;

import java.io.IOException;

@DisplayName("Test of online painter")
public class OnlinePainterPageTest {
    private static WebDriver driver;
    private static OnlinePainterPage onlinePainterPage;

    private static final String ALERT_MESSAGE = "The page was updated and zoomed - this is my Selenium Black Square!";

    @BeforeAll
    public static void setUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        onlinePainterPage = new OnlinePainterPage(driver);
    }

    @Test
    @DisplayName("Test of drawing a square")
    void testOnlinePainter() throws IOException {
        onlinePainterPage.openPage();
        onlinePainterPage.drawSquare();
        onlinePainterPage.clickOnFillInColorByJSExecutor();
        onlinePainterPage.fillInColorForPaintedFigure();
        onlinePainterPage.drawBorderOfFillInBtn();
        onlinePainterPage.takeScreenshot();
        onlinePainterPage.refreshPageByJSExecutor();
        onlinePainterPage.zoomPageByJSExecutor();
        onlinePainterPage.showAnAlertByJSExecutor(ALERT_MESSAGE);
    }

    @AfterAll
    public static void finishTest() {
        driver.quit();
    }
}

package page;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.io.IOException;

public class OnlinePainterPage {
    private final WebDriver driver;
    private static final String PAGE_LINK = "https://jspaint.app/";
    private final Actions actions;

    @FindBy(className = "main-canvas")
    private WebElement canvas;

    @FindBy(css = "div[title='Fill With Color']")
    private WebElement fillWithColorBtn;

    public OnlinePainterPage(WebDriver driver) {
        this.driver = driver;
        driver.manage().window().maximize();
        actions = new Actions(driver);
        PageFactory.initElements(driver, this);
    }

    public void openPage() {
        driver.get(PAGE_LINK);
    }

    public void drawSquare() {

        actions.moveToElement(canvas)
                .clickAndHold()
                .moveByOffset(100, 0)
                .moveByOffset(0, 100)
                .moveByOffset(-100, 0)
                .moveByOffset(0, -100)
                .release()
                .perform();
    }

    public void clickOnFillInColorByJSExecutor() {
        JavaScriptUtil.clickElementByJS(fillWithColorBtn, ((JavascriptExecutor) driver));
    }

    public void fillInColorForPaintedFigure() {
        actions.moveToElement(canvas)
                .moveByOffset(50, 50)
                .click()
                .release()
                .perform();
    }

    public void drawBorderOfFillInBtn() {
        JavaScriptUtil.drawBorder(fillWithColorBtn, ((JavascriptExecutor) driver));
    }

    public void takeScreenshot() throws IOException {
        TakesScreenshot screenshot = (TakesScreenshot) driver;
        File source = screenshot.getScreenshotAs(OutputType.FILE);
        File target = new File(".\\screenshots\\logo.png");
        FileUtils.copyFile(source, target);
    }

    public void showAnAlertByJSExecutor(String alertMessage) {
        JavaScriptUtil.generateAlert(((JavascriptExecutor) driver), alertMessage);
    }

    public void refreshPageByJSExecutor() {
        JavaScriptUtil.refreshBrowser((JavascriptExecutor) driver);
    }

    public void zoomPageByJSExecutor() {
        JavaScriptUtil.zoomPageByJS((JavascriptExecutor) driver);
    }
}
